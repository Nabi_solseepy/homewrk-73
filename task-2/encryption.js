const exspress = require('express');
const Vigenere = require('caesar-salad').Vigenere;


const app = exspress();
const port = 8001;

app.get('/encode/:text', (req, res) => {
    const encode = Vigenere.Cipher('password').crypt(req.params.text);
    res.send(encode)
});

app.get('/decode/:text', (req, res) => {
    const decode = Vigenere.Decipher('password').crypt(req.params.text);
    res.send( decode)
});


app.listen(port);